<%@page import="com.welive.error.portlet.ErrorPortlet"%>
<%@taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%-- Overridden font (HTTPS reference needed) --%>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<%
String messageError = (String)renderRequest.getAttribute(ErrorPortlet.KEY_MESSAGE_ERROR);
%>

<div id="materialize-body" class="materialize">
	<div class="section">
		<h4 class="red-text text-darken-2"><liferay-ui:message key="error.title" /></h4>
		<p class="red-text text-darken-2" id="error.description"><i class="material-icons">error</i>
<%
if(messageError == null) {
%>
		<liferay-ui:message key="error.description" />
<%
} else {
%>
		<%=messageError%>
<%
}
%>
		</p>
	</div>
</div>
