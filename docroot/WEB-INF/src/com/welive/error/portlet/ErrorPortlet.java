package com.welive.error.portlet;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ErrorPortlet
 */
public class ErrorPortlet extends MVCPortlet {
	
	private static final Log _log = LogFactoryUtil.getLog(ErrorPortlet.class.getName());
	
	public static final String KEY_MESSAGE_ERROR = "message_error";

	
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
		String message = httpRequest.getParameter(KEY_MESSAGE_ERROR);
		
		if(message != null) {
			renderRequest.setAttribute(KEY_MESSAGE_ERROR, message);
		}
		
		super.doView(renderRequest, renderResponse);
	}
 

}
